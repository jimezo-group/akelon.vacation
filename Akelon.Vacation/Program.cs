﻿using Akelon.Vacation;

var employes = GenerateEmployes(new string[]
    {
        "Иванов Иван Иванович",
        "Петров Петр Петрович",
        "Юлина Юлия Юлиановна",
        "Сидоров Сидор Сидорович",
        "Павлов Павел Павлович",
        "Георгиев Георг Георгиевич"
    });

DisplayEmployes(employes);

IEnumerable<Employee> GenerateEmployes(params string[] employFullNames)
{
    var random = new Random();
    var employees = new List<Employee>();
    var yearBegin = new DateOnly(DateTime.Today.Year, 1, 1);
    var yearEnd = new DateOnly(DateTime.Today.Year, 12, 31);
    var daysOfYear = (yearEnd.DayNumber - yearBegin.DayNumber);
    
    foreach (var employeeFullName in employFullNames)
    {
        List<DateOnly> employeeVacations = new List<DateOnly>();

        int vacationCount = 28;

        while (vacationCount > 0)
        {
            var generatedDays = random.Next(daysOfYear);
            var startVacationDay = yearBegin.AddDays(generatedDays);

            if (startVacationDay.DayOfWeek == DayOfWeek.Sunday || startVacationDay.DayOfWeek == DayOfWeek.Saturday)
                continue;

            int[] vacationSteps = { 7, 14 };
            int vacationIndex = random.Next(vacationSteps.Length);
            int vacationDays = vacationSteps[vacationIndex];

            if (vacationCount - vacationDays < 0)
                continue;

            var endDate = startVacationDay.AddDays(vacationDays);

            var hasVacationIntervalOfMonth = employeeVacations.Any(element => element.AddMonths(1) >= startVacationDay && element.AddMonths(1) >= endDate) &&
                                   employeeVacations.Any(element => element.AddMonths(-1) <= startVacationDay && element.AddMonths(-1) <= endDate);

            if (hasVacationIntervalOfMonth == false)
            {
                for (var date = startVacationDay; date < endDate; date = date.AddDays(1))
                {
                    employeeVacations.Add(date);
                }

                vacationCount -= vacationDays;
            }
        }

        employees.Add(new Employee(employeeFullName, employeeVacations));
    }

    return employees;
}

void DisplayEmployes(IEnumerable<Employee> employees)
{
    foreach (var employee in employees)
    {
        Console.WriteLine($"Дни отпуска {employee.FullName}: ");

        foreach (var vacationDay in employee.VacationDays)
        {
            Console.WriteLine(vacationDay);
        }
    }

    Console.ReadKey();
}
﻿namespace Akelon.Vacation
{
    internal sealed class Employee
    {
        private readonly string _fullName;
        private readonly IEnumerable<DateOnly> _vacationDays;

        public Employee(string fullName, IEnumerable<DateOnly> vacationDays)
        {
            _fullName = fullName;
            _vacationDays = vacationDays;
        }

        public string FullName => _fullName;
        public DateOnly[] VacationDays => _vacationDays.ToArray();
    }
}
